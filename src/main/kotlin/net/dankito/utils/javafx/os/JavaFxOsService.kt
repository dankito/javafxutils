package net.dankito.utils.javafx.os

import org.slf4j.LoggerFactory
import java.awt.Desktop
import java.io.File
import kotlin.concurrent.thread


// TODO: implement IApplicationsService
open class JavaFxOsService {

    companion object {
        private val log = LoggerFactory.getLogger(JavaFxOsService::class.java)
    }


    open fun openFileInOsDefaultApplication(file: File) {
        openFile(file)
    }

    open fun openDirectoryInOsFileBrowser(file: File) {
        openFile(file)
    }


    protected open fun openFile(file: File) {
        thread { // get off UI thread
            try {
                Desktop.getDesktop().open(file)
            } catch(e: Exception) { log.error("Could not show file $file", e) }
        }
    }

}