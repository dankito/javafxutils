package net.dankito.utils.javafx.ui.extensions

import javafx.beans.value.ObservableValue
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import tornadofx.cellFormat
import tornadofx.column
import java.text.DateFormat
import java.text.NumberFormat
import kotlin.reflect.KProperty1


class TableViewExtensions {

    companion object {

        const val DateColumnPrefWidth = 80.0

        val DateTimeFormat = DateFormat.getDateInstance(DateFormat.SHORT)


        const val CurrencyColumnPrefWidth = 110.0

        val CurrencyNumberFormat = NumberFormat.getCurrencyInstance()

    }

}


/**
 * Create a column with default values set suitable for columns displaying a date:
 * Cell alignment gets set to center and cell text gets formatted with DateFormat.getDateInstance(DateFormat.SHORT).
 */
inline fun <reified S, T> TableView<S>.dateColumn(title: String,
                                                  prop: KProperty1<S, T>,
                                                  dateFormat: DateFormat = TableViewExtensions.DateTimeFormat,
                                                  noinline op: TableColumn<S, T>.() -> Unit = {}): TableColumn<S, T> {

    return this.column(title, prop, op).apply {
        prefWidth = TableViewExtensions.DateColumnPrefWidth

        addStyleToCurrentStyle("-fx-alignment: CENTER;")

        cellFormat { text = dateFormat.format(it) }
    }
}

/**
 * Create a column with default values set suitable for columns displaying a concurrency:
 * Cell alignment gets set to center right and cell text gets formatted with NumberFormat.getCurrencyInstance().
 */
inline fun <reified S> TableView<S>.currencyColumn(title: String,
                                                   prop: KProperty1<S, Number>,
                                                   concurrencyNumberFormat: NumberFormat = TableViewExtensions.CurrencyNumberFormat,
                                                   noinline op: TableColumn<S, Number>.() -> Unit = {}): TableColumn<S, Number> {

    return this.column(title, prop, op).apply {
        setupCurrencyColumn(concurrencyNumberFormat)
    }
}

/**
 * Create a column with default values set suitable for columns displaying a concurrency:
 * Cell alignment gets set to center right and cell text gets formatted with NumberFormat.getCurrencyInstance().
 */
@JvmName(name = "columnForObservableProperty")
inline fun <reified S> TableView<S>.currencyColumn(title: String,
                                                   prop: KProperty1<S, ObservableValue<Number>>,
                                                   concurrencyNumberFormat: NumberFormat = TableViewExtensions.CurrencyNumberFormat,
                                                   noinline op: TableColumn<S, Number>.() -> Unit = {}): TableColumn<S, Number> {

    return this.column(title, prop, op).apply {
        setupCurrencyColumn(concurrencyNumberFormat)
    }
}

inline fun <reified S> TableColumn<S, Number>.setupCurrencyColumn(concurrencyNumberFormat: NumberFormat) {
    prefWidth = TableViewExtensions.CurrencyColumnPrefWidth

    addStyleToCurrentStyle("-fx-alignment: CENTER-RIGHT;")

    cellFormat { text = concurrencyNumberFormat.format(it) }
}