package net.dankito.utils.javafx.ui

import javafx.stage.Stage
import net.dankito.utils.info.SystemProperties
import net.dankito.utils.javafx.ui.dialogs.JavaFXDialogService
import net.dankito.utils.localization.UTF8ResourceBundleControl
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tornadofx.App
import tornadofx.FX
import tornadofx.Stylesheet
import tornadofx.UIComponent
import java.util.*
import kotlin.reflect.KClass


/**
 * [FX.messages] by default doesn't support UTF-8.
 *
 * So this class sets up UTF-8 support by setting [FX.messages] to a resource bundle with name
 * [messageResourcesFilename] and using [UTF8ResourceBundleControl].
 *
 * Also calls System.exit(0) in [stop()] method so that application really gets stopped when closing last window.
 *
 * If this is undesired simply overwrite [stop] and don't call `super.stop()`.
 */
open class Utf8App(protected val messageResourcesFilename: String, primaryView: KClass<out UIComponent>? = null,
              vararg stylesheet: KClass<out Stylesheet>): App(primaryView, *stylesheet) {


    override fun start(stage: Stage) {
        try {
            fixStartupDirectory()

            beforeStart(stage)

            setupMessagesResources() // has to be done before creating / injecting first instances as some of them already rely on Messages

            super.start(stage)

            afterStart(stage)
        } catch (e: Exception) {
            getLogger().error("Could not start application", e)

            showApplicationStartFailedErrorMessage(e)

            throw e
        }
    }


    protected open fun fixStartupDirectory() {
        SystemProperties().fixStartupDirectory()
    }


    protected open fun showApplicationStartFailedErrorMessage(e: Exception) {
        // TODO: translate error message
        JavaFXDialogService()
            .showErrorMessage("Error during application startup", "Could not start application", e)
    }


    /*      Hooks for child classes         */

    protected open fun beforeStart(primaryStage: Stage) {

    }

    protected open fun afterStart(primaryStage: Stage) {

    }


    protected open fun setupMessagesResources() {
        ResourceBundle.clearCache() // at this point default ResourceBundles are already created and cached. In order that ResourceBundle created below takes effect cache has to be cleared before

        FX.messages = ResourceBundle.getBundle(messageResourcesFilename, UTF8ResourceBundleControl())
    }


    override fun stop() {
        super.stop()
        System.exit(0) // otherwise main window would be closed but application still running in background
    }


    protected open fun getLogger(): Logger {
        return LoggerFactory.getLogger(Utf8App::class.java)
    }

}