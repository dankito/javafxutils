package net.dankito.utils.javafx.ui.image

import net.dankito.utils.image.ImageReference
import java.io.File


class JavaFXImageReference(val imageUrl: String): ImageReference {

    companion object {

        fun fromIconsResourceName(iconResourceName: String): JavaFXImageReference {
            return fromResourceName("icons/", iconResourceName)
        }

        fun fromResourceName(resourceFolder: String, resourceName: String): JavaFXImageReference {
            return JavaFXImageReference(JavaFXImageReference::class.java.classLoader.getResource(File(resourceFolder, resourceName).path).toExternalForm())
        }

    }

}