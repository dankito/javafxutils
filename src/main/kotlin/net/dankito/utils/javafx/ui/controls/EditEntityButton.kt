package net.dankito.utils.javafx.ui.controls

import javafx.event.EventTarget
import javafx.scene.control.Button
import javafx.scene.control.MenuItem
import javafx.scene.control.SplitMenuButton
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import net.dankito.utils.javafx.ui.extensions.ensureOnlyUsesSpaceIfVisible
import net.dankito.utils.localization.Localization
import tornadofx.action
import tornadofx.add
import tornadofx.opcr
import tornadofx.useMaxSize


fun EventTarget.editEntityButton(editClicked: () -> Unit, createClicked: () -> Unit, deleteClicked: (() -> Unit)? = null, op: EditEntityButton.() -> Unit = {})
        = opcr(this, EditEntityButton(Localization("JavaFXUtils_Translations"), editClicked, createClicked, deleteClicked), op)

fun EventTarget.editEntityButton(localization: Localization, editClicked: () -> Unit, createClicked: () -> Unit, deleteClicked: (() -> Unit)? = null, op: EditEntityButton.() -> Unit = {})
        = opcr(this, EditEntityButton(localization, editClicked, createClicked, deleteClicked), op)

fun EventTarget.editEntityButton(editLabel: String, editClicked: () -> Unit, createLabel: String, createClicked: () -> Unit,
                                 deleteLabel: String? = null, deleteClicked: (() -> Unit)? = null, op: EditEntityButton.() -> Unit = {})
        = opcr(this, EditEntityButton(editLabel, editClicked, createLabel, createClicked, deleteLabel, deleteClicked), op)


open class EditEntityButton(
        protected val editLabel: String,
        protected val editClicked: () -> Unit,
        protected val createLabel: String,
        protected val createClicked: () -> Unit,
        protected val deleteLabel: String? = null,
        protected val deleteClicked: (() -> Unit)? = null
) : HBox() {

    constructor(editClicked: () -> Unit, createClicked: () -> Unit, deleteClicked: (() -> Unit)? = null)
            : this(Localization("JavaFXUtils_Translations"), editClicked, createClicked, deleteClicked)

    constructor(localization: Localization, editClicked: () -> Unit, createClicked: () -> Unit, deleteClicked: (() -> Unit)? = null)
            : this(localization.getLocalizedString("edit..."), editClicked,
                localization.getLocalizedString("create..."), createClicked,
                localization.getLocalizedString("delete..."), deleteClicked)


    protected val multipleOperationsButton = SplitMenuButton()

    protected val createOperationOnlyButton = Button(createLabel)


    open var showOnlyCreateOperation: Boolean = false
        set(value) {
            if (field != value) {
                field = value

                showOnlyCreateOperationChanged(value)
            }
        }


    init {
        initView()
    }


    protected open fun initCreateOperationOnlyButton() {
        createOperationOnlyButton.action { createClicked() }
        createOperationOnlyButton.useMaxSize = true
        setHgrow(createOperationOnlyButton, Priority.ALWAYS)
        createOperationOnlyButton.ensureOnlyUsesSpaceIfVisible()

        createOperationOnlyButton.isVisible = false

        this.add(createOperationOnlyButton)
    }

    protected open fun initView() {
        prefWidth = 135.0

        initCreateOperationOnlyButton()

        initMultipleOperationsButton(deleteClicked)
    }

    protected open fun initMultipleOperationsButton(deleteClicked: (() -> Unit)?) {
        multipleOperationsButton.text = editLabel
        multipleOperationsButton.useMaxSize = true
        setHgrow(multipleOperationsButton, Priority.ALWAYS)
        multipleOperationsButton.ensureOnlyUsesSpaceIfVisible()

        multipleOperationsButton.action { editClicked() }

        val createMenu = MenuItem(createLabel)
        createMenu.action { createClicked() }
        multipleOperationsButton.items.add(createMenu)

        if (deleteClicked != null) {
            val deleteMenu = MenuItem(deleteLabel)
            deleteMenu.action { deleteClicked.invoke() }
            multipleOperationsButton.items.add(deleteMenu)
        }

        this.add(multipleOperationsButton)
    }


    protected open fun showOnlyCreateOperationChanged(showOnlyCreate: Boolean) {
        if (showOnlyCreate) {
            createOperationOnlyButton.isVisible = true
            multipleOperationsButton.isVisible = false
        }
        else {
            createOperationOnlyButton.isVisible = false
            multipleOperationsButton.isVisible = true
        }
    }

}