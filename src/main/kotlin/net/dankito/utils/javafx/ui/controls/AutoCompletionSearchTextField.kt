package net.dankito.utils.javafx.ui.controls

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.scene.control.ContextMenu
import javafx.scene.control.TextField
import tornadofx.ListCellFragment
import tornadofx.bind
import tornadofx.opcr
import kotlin.reflect.KClass


fun <T> EventTarget.autocompletionsearchtextfield(value: String? = null, op: AutoCompletionSearchTextField<T>.() -> Unit = {}) =
        opcr(this, AutoCompletionSearchTextField<T>().apply { if (value != null) text = value }, op)

fun <T> EventTarget.autocompletionsearchtextfield(property: ObservableValue<String>, op: AutoCompletionSearchTextField<T>.() -> Unit = {}): AutoCompletionSearchTextField<T>
        = autocompletionsearchtextfield<T>().apply {
    bind(property)
    op(this)
}



open class AutoCompletionSearchTextField<T> : TextField() {

    open var onAutoCompletion: ((T) -> Unit)? = null

    open var getContextMenuForItemListener: ((item: T) -> ContextMenu?)?
        get() { return autoCompletionBinding.getContextMenuForItemListener }
        set(value) { autoCompletionBinding.getContextMenuForItemListener = value }


    open var listCellFragment: KClass<out ListCellFragment<T>>?
        get() { return autoCompletionBinding.listCellFragment }
        set(value) { autoCompletionBinding.listCellFragment = value }


    protected var autoCompletionBinding = AutoCompletionBinding<T>(this)


    init {
        autoCompletionBinding.setOnAutoCompleted { e -> onAutoCompletion?.invoke(e.completion) }

        autoCompletionBinding.prefWidthProperty().bind(widthProperty())

        var previousY = 0.0

        boundsInParentProperty().addListener { _, _, newValue ->
            if(newValue.minY != previousY) {
                previousY = newValue.minY

                updateSuggestionsListPosition()
            }
        }
    }


    open fun setAutoCompleteList(autoCompletionList: Collection<T>, queryToSelectFromAutoCompletionList: String = text) {
        autoCompletionBinding.setAutoCompleteList(autoCompletionList, queryToSelectFromAutoCompletionList)
    }

    open fun setPrefItemHeight(prefHeight: Double) {
        autoCompletionBinding.setPrefItemHeight(prefHeight)
    }

    protected open fun updateSuggestionsListPosition() {
        autoCompletionBinding.updatePopupPosition()
    }

}