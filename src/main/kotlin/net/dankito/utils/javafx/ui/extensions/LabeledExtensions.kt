package net.dankito.utils.javafx.ui.extensions

import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Labeled
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import net.dankito.utils.javafx.ui.constants.Fonts
import net.dankito.utils.javafx.ui.constants.Texts
import net.dankito.utils.javafx.ui.constants.UiColors


fun Labeled.styleContentAsAddButton(addText: String = Texts.DefaultAddText, textColorHex: String = UiColors.AddButtonHexColor, fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize) {
    styleContentAsAddOrRemoveButton(addText, textColorHex, fontSize)
}

fun Labeled.styleContentAsRemoveButton(removeText: String = Texts.DefaultRemoveText, textColorHex: String = UiColors.AddButtonHexColor, fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize) {
    styleContentAsAddOrRemoveButton(removeText, textColorHex, fontSize)
}

private fun Labeled.styleContentAsAddOrRemoveButton(text: String, textColorHex: String, fontSize: Double) {
    this.text = text

    font = Font.font(font.family, FontWeight.BOLD, fontSize)
    textFill = Color.valueOf(textColorHex)

    alignment = Pos.CENTER
    contentDisplay = ContentDisplay.TEXT_ONLY
}