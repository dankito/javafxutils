package net.dankito.utils.javafx.ui.extensions

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.CancellationException
import kotlin.coroutines.CoroutineContext


class Coroutines {

    companion object {
        internal val log = LoggerFactory.getLogger(Coroutines::class.java)
    }

}


fun <T> runNonBlockingDispatchToUiThread(errorLogMessage: String? = null, log: Logger = Coroutines.log,
                                         action: suspend () -> T?, resultOnUiThread: (T) -> Unit) {
    runNonBlockingDispatchToUiThread(action, Dispatchers.IO, errorLogMessage, log, resultOnUiThread)
}

fun <T> runNonBlockingDispatchToUiThread(action: suspend () -> T?, context: CoroutineContext = Dispatchers.IO, errorLogMessage: String? = null, log: Logger = Coroutines.log,
                                         resultOnUiThread: (T) -> Unit) = GlobalScope.launch(context) {
    try {
        val result = action()

        result?.let {
            withContext(Dispatchers.JavaFx) {
                resultOnUiThread(result)
            }
        }
    } catch (e: Exception) {
        if (e is CancellationException == false) {
            log.error(errorLogMessage ?: "Could not execute async action", e)
        }
    }
}