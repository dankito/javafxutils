package net.dankito.utils.javafx.ui.extensions

import tornadofx.UIComponent


fun UIComponent.updateWindowSize() {
    currentStage?.sizeToScene()
}