package net.dankito.utils.javafx.ui.controls

import javafx.beans.value.ObservableValue
import javafx.scene.control.TextField
import javafx.util.StringConverter
import tornadofx.bind


abstract class NumberTextFieldBase(propertyToBind: ObservableValue<Number>? = null) : TextField() {


    protected abstract fun isTextInputAllowed(text: String): Boolean

    protected abstract fun convertToString(value: Number): String

    protected abstract fun convertFromString(value: String): Number?



    init {
        textProperty().addListener { _, oldValue, newValue ->
            if (isTextInputAllowed(newValue) == false) {
                this.text = oldValue
            }
        }

        bind(propertyToBind)
    }

    protected open fun bind(propertyToBind: ObservableValue<Number>?) {
        propertyToBind?.let {
            bind(propertyToBind, false, createStringConverter(), null)
        }
    }


    protected open fun createStringConverter(): StringConverter<Number> {
        return object : StringConverter<Number>() {
            override fun toString(value: Number): String {
                return convertToString(value)
            }

            override fun fromString(value: String): Number? {
                return convertFromString(value)
            }

        }
    }

}