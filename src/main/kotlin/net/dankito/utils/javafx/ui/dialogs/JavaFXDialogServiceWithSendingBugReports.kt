package net.dankito.utils.javafx.ui.dialogs

import javafx.scene.control.Alert
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.stage.Stage
import net.dankito.utils.localization.Localization
import org.slf4j.LoggerFactory
import java.awt.Desktop
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URI
import java.net.URLEncoder
import kotlin.concurrent.thread


open class JavaFXDialogServiceWithSendingBugReports(
    localizedStacktraceLabel: String,
    protected val sendBugReportButtonText: String,
    protected val emailAddressForBugReports: String,
    protected val subjectForBugReports: String
) : JavaFXDialogService(localizedStacktraceLabel) {

    companion object {
        private val log = LoggerFactory.getLogger(JavaFXDialogServiceWithSendingBugReports::class.java)
    }


    @JvmOverloads
    constructor(localization: Localization = Localization("JavaFXUtils_Translations"), emailAddressForBugReports: String, subjectForBugReports: String)
            : this(localization.getLocalizedString("dialog.service.error.stacktrace.label"),
                    localization.getLocalizedString("dialog.service.message.send.bug.report"),
                    emailAddressForBugReports, subjectForBugReports)



    override fun createExpandableException(alert: Alert, exception: Exception) {
        super.createExpandableException(alert, exception)

        val sendBugReportButton = ButtonType(sendBugReportButtonText, ButtonBar.ButtonData.OTHER)
        alert.buttonTypes.add(sendBugReportButton)
    }

    override fun showErrorMessageOnUiThread(errorMessage: CharSequence, alertTitle: CharSequence?, exception: Exception?, owner: Stage?): ButtonType {
        val clickedButton = super.showErrorMessageOnUiThread(errorMessage, alertTitle, exception, owner)

        if (clickedButton.buttonData == ButtonBar.ButtonData.OTHER && exception != null) {
            sendBugReport(exception, errorMessage)
        }

        return clickedButton
    }

    protected open fun sendBugReport(exception: Exception, errorMessage: CharSequence) {
        if(emailAddressForBugReports != null) {
            thread { // get off UI thread
                try {
                    val mailUri = createMailUriForBugReport(errorMessage, exception)

                    Desktop.getDesktop().mail(URI.create(mailUri))
                } catch(e: Exception) { log.error("Could not send bug report", e) }
            }
        }
    }

    protected open fun createMailUriForBugReport(errorMessage: CharSequence, exception: Exception): String {
        var bugReport = errorMessage.toString() + "\r\n\r\n"

        val sw = StringWriter()
        val pw = PrintWriter(sw)
        exception.printStackTrace(pw)
        val stackTrace = sw.toString()
        pw.close()

        bugReport += "Stack trace:\r\n\r\n$stackTrace"

        var mailUri = "mailto:$emailAddressForBugReports?"

        subjectForBugReports?.let {
            mailUri += "subject=${URLEncoder.encode(it, "utf-8").replace("+", "%20")}&" // use %20, not URLEncoder's + as Thunderbird understands only %20
        }

        val encodedBugReport = URLEncoder.encode(bugReport, "utf-8").replace("+", "%20")
        mailUri += "body=$encodedBugReport"

        return mailUri
    }

}