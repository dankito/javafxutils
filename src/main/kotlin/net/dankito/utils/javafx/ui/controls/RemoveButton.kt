package net.dankito.utils.javafx.ui.controls

import javafx.event.EventTarget
import javafx.scene.control.Button
import net.dankito.utils.javafx.ui.constants.Fonts
import net.dankito.utils.javafx.ui.constants.Texts
import net.dankito.utils.javafx.ui.constants.UiColors
import net.dankito.utils.javafx.ui.extensions.styleContentAsRemoveButton
import tornadofx.opcr


fun EventTarget.removeButton(removeText: String = Texts.DefaultRemoveText, textColorHex: String = UiColors.RemoveButtonHexColor,
                             fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize, op: RemoveButton.() -> Unit = {})
        = opcr(this, RemoveButton(removeText, textColorHex, fontSize), op)


open class RemoveButton(removeText: String = Texts.DefaultRemoveText, textColorHex: String = UiColors.RemoveButtonHexColor, fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize)
    : Button() {

    init {
        styleContentAsRemoveButton(removeText, textColorHex, fontSize)
    }

}