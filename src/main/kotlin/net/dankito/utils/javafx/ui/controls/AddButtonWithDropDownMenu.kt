package net.dankito.utils.javafx.ui.controls

import javafx.event.EventTarget
import javafx.scene.control.SplitMenuButton
import net.dankito.utils.javafx.ui.constants.Fonts
import net.dankito.utils.javafx.ui.constants.Texts
import net.dankito.utils.javafx.ui.constants.UiColors
import net.dankito.utils.javafx.ui.extensions.setDropDownButtonWidth
import net.dankito.utils.javafx.ui.extensions.styleContentAsAddButton
import tornadofx.opcr


fun EventTarget.addButtonWithDropDownMenu(addText: String = Texts.DefaultAddText, textColorHex: String = UiColors.AddButtonHexColor,
                                          fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize, dropDownButtonWidth: Double = AddButtonWithDropDownMenu.DefaultDropDownButtonWidth,
                                          op: AddButtonWithDropDownMenu.() -> Unit = {})
        = opcr(this, AddButtonWithDropDownMenu(addText, textColorHex, fontSize, dropDownButtonWidth), op)


open class AddButtonWithDropDownMenu(addText: String = Texts.DefaultAddText, textColorHex: String = UiColors.AddButtonHexColor,
                                     fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize, dropDownButtonWidth: Double = DefaultDropDownButtonWidth)
    : SplitMenuButton() {

    init {
        styleContentAsAddButton(addText, textColorHex, fontSize)

        setDropDownButtonWidth(dropDownButtonWidth)
    }


    companion object {
        const val DefaultDropDownButtonWidth = 20.0
    }

}