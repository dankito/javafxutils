package net.dankito.utils.javafx.ui.extensions

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.scene.control.SelectionModel


@JvmOverloads
fun <T> SelectionModel<T>.bindSelectedItemTo(observableValue: ObjectProperty<T>, selectedItemChanged: ((T?) -> Unit)? = null) {
    selectedItemProperty().addListener { _, _, newValue ->
        observableValue.value = newValue

        selectedItemChanged?.invoke(newValue)
    }

    observableValue.addListener { _, _, newValue ->
        select(newValue)
    }

    observableValue.value?.let {
        select(it)
    }
}

fun <T> SelectionModel<T>.bindIsAnItemSelectedTo(observableValue: BooleanProperty) {
    selectedItemProperty().addListener { _, _, newValue ->
        observableValue.value = newValue != null
    }

    observableValue.value = selectedItem != null
}