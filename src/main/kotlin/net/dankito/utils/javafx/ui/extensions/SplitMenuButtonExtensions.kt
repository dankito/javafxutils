package net.dankito.utils.javafx.ui.extensions

import com.sun.javafx.scene.control.skin.MenuButtonSkinBase
import com.sun.javafx.scene.control.skin.SplitMenuButtonSkin
import javafx.scene.control.SplitMenuButton
import javafx.scene.layout.Region
import org.slf4j.LoggerFactory
import tornadofx.paddingHorizontal


class SplitMenuButtonExtensions {

    companion object {
        internal val log = LoggerFactory.getLogger(SplitMenuButtonExtensions::class.java)
    }

}


fun SplitMenuButton.setDropDownButtonWidth(width: Double) {
    skinProperty().addListener { _, _, skin ->
        (skin as? SplitMenuButtonSkin)?.let { skin ->
            try {
                val arrowButtonField = MenuButtonSkinBase::class.java.getDeclaredField("arrowButton")

                arrowButtonField.isAccessible = true

                val arrowButton = arrowButtonField.get(skin) as Region
                arrowButton.paddingHorizontal = 4.0
                arrowButton.minWidth = width
                arrowButton.maxWidth = width
            } catch (e: Exception) {
                SplitMenuButtonExtensions.log.error("Could not set arrow buttons width", e)
            }
        }
    }
}