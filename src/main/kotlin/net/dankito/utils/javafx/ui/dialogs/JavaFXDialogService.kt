package net.dankito.utils.javafx.ui.dialogs

import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.stage.Screen
import javafx.stage.Stage
import net.dankito.utils.javafx.util.FXUtils
import net.dankito.utils.localization.Localization
import net.dankito.utils.ui.dialogs.ConfirmationDialogButton
import net.dankito.utils.ui.dialogs.ConfirmationDialogConfig
import net.dankito.utils.ui.dialogs.IDialogService
import net.dankito.utils.ui.dialogs.InputType
import tornadofx.runLater
import tornadofx.useMaxHeight
import tornadofx.useMaxWidth
import java.io.PrintWriter
import java.io.StringWriter


open class JavaFXDialogService(protected val localizedStacktraceLabel: String) : IDialogService {

    @JvmOverloads
    constructor(localization: Localization = Localization("JavaFXUtils_Translations")) : this(localization.getLocalizedString("dialog.service.error.stacktrace.label"))


    override fun showLittleInfoMessage(infoMessage: CharSequence) {
        // nothing to do for JavaFX, was only introduced for showing Android Toasts
    }

    override fun showInfoMessage(infoMessage: CharSequence, alertTitle: CharSequence?) {
        showInfoMessage(infoMessage, alertTitle, null)
    }

    fun showInfoMessage(infoMessage: CharSequence, alertTitle: CharSequence?, owner: Stage?) {
        runLater { showInfoMessageOnUiThread(infoMessage, alertTitle, owner) }
    }

    protected open fun showInfoMessageOnUiThread(infoMessage: CharSequence, alertTitle: CharSequence?, owner: Stage?) {
        val alert = createDialog(Alert.AlertType.INFORMATION, infoMessage, alertTitle, owner, ButtonType.OK)

        alert.showAndWait()
    }


    override fun showConfirmationDialog(message: CharSequence, alertTitle: CharSequence?, config: ConfirmationDialogConfig, optionSelected: (ConfirmationDialogButton) -> Unit) {
        showConfirmationDialog(message, alertTitle, config, null, optionSelected)
    }

    fun showConfirmationDialog(message: CharSequence, alertTitle: CharSequence?, config: ConfirmationDialogConfig = ConfirmationDialogConfig(), owner: Stage?, optionSelected: (ConfirmationDialogButton) -> Unit) {
        runLater { showConfirmationDialogOnUiThread(message, alertTitle, config, owner, optionSelected) }
    }

    protected open fun showConfirmationDialogOnUiThread(message: CharSequence, alertTitle: CharSequence?, config: ConfirmationDialogConfig, owner: Stage?, optionSelected: (ConfirmationDialogButton) -> Unit) {
        val buttons = ArrayList<ButtonType>()
        if(config.showNoButton) {
            buttons.add(ButtonType.NO)
        }
        if(config.showThirdButton) {
            buttons.add(ButtonType.CANCEL) // TODO: test
        }
        buttons.add(ButtonType.YES)

        val alert = createDialog(Alert.AlertType.CONFIRMATION, message, alertTitle, owner, *buttons.toTypedArray())

        when(alert.showAndWait().get()) {
            ButtonType.NO -> return optionSelected(ConfirmationDialogButton.No)
            else -> return optionSelected(ConfirmationDialogButton.Confirm)
        }
    }


    override fun showErrorMessage(errorMessage: CharSequence, alertTitle: CharSequence?, exception: Exception?) {
        showErrorMessage(errorMessage, alertTitle, exception, null)
    }

    open fun showErrorMessage(errorMessage: CharSequence, alertTitle: CharSequence?, exception: Exception?, owner: Stage?) {
        runLater { showErrorMessageOnUiThread(errorMessage, alertTitle, exception, owner) }
    }

    protected open fun showErrorMessageOnUiThread(errorMessage: CharSequence, alertTitle: CharSequence?, exception: Exception?, owner: Stage?): ButtonType {
        val alert = createDialog(Alert.AlertType.ERROR, errorMessage, alertTitle, owner, ButtonType.OK)
        alert.isResizable = true

        if (exception != null) {
            createExpandableException(alert, exception)
        }

        return alert.showAndWait().get()
    }

    protected open fun createExpandableException(alert: Alert, exception: Exception) {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        exception.printStackTrace(pw)
        val exceptionText = sw.toString()

        val label = Label(localizedStacktraceLabel)

        val textArea = TextArea(exceptionText)
        textArea.isEditable = false
        textArea.isWrapText = true

        textArea.useMaxWidth = true
        textArea.useMaxHeight = true
        GridPane.setVgrow(textArea, Priority.ALWAYS)
        GridPane.setHgrow(textArea, Priority.ALWAYS)

        val expContent = GridPane()
        expContent.useMaxWidth = true
        expContent.add(label, 0, 0)
        expContent.add(textArea, 0, 1)

        configureExpandableException(alert)

        // Set expandable Exception into the dialog pane.
        alert.dialogPane.expandableContent = expContent
    }

    protected open fun configureExpandableException(alert: Alert) {
        // may be overridden and used in sub classes
    }


    protected open fun createDialog(alertType: Alert.AlertType, message: CharSequence, alertTitle: CharSequence?, owner: Stage?, vararg buttons: ButtonType): Alert {
        val alert = Alert(alertType)

        (alertTitle as? String)?.let { alert.title = it }

        owner?.let { alert.initOwner(it) }

        (message as? String)?.let { setAlertContent(alert, it) }
        alert.headerText = null

        alert.buttonTypes.setAll(*buttons)

        return alert
    }

    protected open fun setAlertContent(alert: Alert, content: String) {
        var maxWidth = Screen.getPrimary().visualBounds.width

        if(alert.owner != null) {
            FXUtils.getScreenWindowLeftUpperCornerIsIn(alert.owner)?.let { ownersScreen ->
                maxWidth = ownersScreen.visualBounds.width
            }
        }

        maxWidth *= 0.6 // set max width to 60 % of Screen width

        val contentLabel = Label(content)
        contentLabel.isWrapText = true
        contentLabel.prefHeight = Region.USE_COMPUTED_SIZE
        contentLabel.useMaxHeight = true
        contentLabel.maxWidth = maxWidth

        val contentPane = VBox(contentLabel)
        contentPane.prefHeight = Region.USE_COMPUTED_SIZE
        contentPane.useMaxHeight = true
        VBox.setVgrow(contentLabel, Priority.ALWAYS)

        alert.dialogPane.prefHeight = Region.USE_COMPUTED_SIZE
        alert.dialogPane.useMaxHeight = true
        alert.dialogPane.maxWidth = maxWidth
        alert.dialogPane.content = contentPane
    }


    override fun askForTextInput(questionText: CharSequence, alertTitleText: CharSequence?, defaultValue: CharSequence?, type: InputType, callback: (Boolean, String?) -> Unit) {
        runLater { askForTextInputOnUiThread(questionText, alertTitleText, defaultValue, callback) }
    }

    protected open fun askForTextInputOnUiThread(questionText: CharSequence, alertTitleText: CharSequence?, defaultValue: CharSequence?, callback: (Boolean, String?) -> Unit) {
        val dialog = TextInputDialog(defaultValue as? String)
        dialog.headerText = null
        dialog.title = alertTitleText as? String
        dialog.contentText = questionText as? String

        val result = dialog.showAndWait()

        val enteredText = if(result.isPresent) result.get() else null
        callback(result.isPresent, enteredText)
    }

}