package net.dankito.utils.javafx.ui.constants


class UiColors {

    companion object {

        const val AddButtonHexColor = "#117219"

        const val RemoveButtonHexColor = "#e90b1f"

    }

}