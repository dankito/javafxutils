package net.dankito.utils.javafx.ui.extensions

import javafx.geometry.Insets
import javafx.scene.layout.*
import javafx.scene.paint.Color
import tornadofx.UIComponent


var Region.fixedHeight: Double
    get() { throw NoSuchMethodException("fixedHeight 'Property' is only a shortcut for setting a fixed height, so only setter is implemented") }
    set(value) {
        this.minHeight = value
        this.maxHeight = value
    }

var Region.fixedWidth: Double
    get() { throw NoSuchMethodException("fixedWidth 'Property' is only a shortcut for setting a fixed width, so only setter is implemented") }
    set(value) {
        this.minWidth = value
        this.maxWidth = value
    }


fun UIComponent.setBackgroundToColorIfIsRegion(color: Color, radii: CornerRadii = CornerRadii.EMPTY, insets: Insets = Insets.EMPTY) {
    (this.root as? Region)?.setBackgroundToColor(color, radii, insets)
}

fun Region.setBackgroundToColor(color: Color, radii: CornerRadii = CornerRadii.EMPTY, insets: Insets = Insets.EMPTY) {
    this.background = Background(BackgroundFill(color, radii, insets))
}

fun Region.setBorder(color: Color = Color.DARKGRAY, borderWidths: Double = 2.0, cornerRadii: Double = 8.0, strokeStyle: BorderStrokeStyle = BorderStrokeStyle.SOLID) {
    this.border = Border(BorderStroke(color, strokeStyle, CornerRadii(cornerRadii), BorderWidths(borderWidths)))
}