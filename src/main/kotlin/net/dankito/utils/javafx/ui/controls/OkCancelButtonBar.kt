package net.dankito.utils.javafx.ui.controls

import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.layout.AnchorPane
import net.dankito.utils.javafx.ui.extensions.fixedHeight
import net.dankito.utils.javafx.ui.extensions.fixedWidth
import tornadofx.*
import tornadofx.FX.Companion.messages


fun EventTarget.okCancelButtonBar(buttonsHeight: Double,
                                  buttonsWidth: Double,
                                  cancelButtonClicked: () -> Unit,
                                  okButtonClicked: () -> Unit,
                                  enableOkButton: BooleanProperty = SimpleBooleanProperty(true),
                                  okButtonCaption: String = messages["ok"],
                                  cancelButtonCaption: String = messages["cancel"],
                                  thirdButton: Node? = null,
                                  op: OkCancelButtonBar.() -> Unit = {})
        = opcr(this, OkCancelButtonBar(buttonsHeight, buttonsWidth, cancelButtonClicked, okButtonClicked, enableOkButton, okButtonCaption, cancelButtonCaption, thirdButton), op)

open class OkCancelButtonBar(
    buttonsHeight: Double,
    buttonsWidth: Double,
    cancelButtonClicked: () -> Unit,
    okButtonClicked: () -> Unit,
    enableOkButton: BooleanProperty = SimpleBooleanProperty(true),
    okButtonCaption: String = messages["ok"],
    cancelButtonCaption: String = messages["cancel"],
    thirdButton: Node? = null
) : AnchorPane() {


    init {
        initView(buttonsHeight, buttonsWidth, cancelButtonClicked, okButtonClicked, enableOkButton, okButtonCaption, cancelButtonCaption, thirdButton)
    }

    protected open fun initView(
        buttonsHeight: Double,
        buttonsWidth: Double,
        cancelButtonClicked: () -> Unit,
        okButtonClicked: () -> Unit,
        enableOkButton: BooleanProperty,
        okButtonCaption: String,
        cancelButtonCaption: String,
        thirdButton: Node?
    ) {
        fixedHeight = buttonsHeight
        useMaxWidth = true

        button(cancelButtonCaption) {
            fixedWidth = buttonsWidth

            isCancelButton = true

            action { cancelButtonClicked.invoke() }

            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = buttonsWidth + 12.0
                bottomAnchor = 0.0
            }
        }

        button(okButtonCaption) {
            fixedWidth = buttonsWidth

            isDefaultButton = true

            enableWhen(enableOkButton)

            action { okButtonClicked() }

            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
            }
        }

        thirdButton?.let {
            add(thirdButton).apply {

                anchorpaneConstraints {
                    topAnchor = 0.0
                    leftAnchor = 0.0
                    bottomAnchor = 0.0
                }
            }
        }
    }
}