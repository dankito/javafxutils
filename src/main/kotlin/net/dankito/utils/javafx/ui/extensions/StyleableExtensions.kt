package net.dankito.utils.javafx.ui.extensions

import javafx.css.Styleable


fun Styleable.appendStyleToCurrentStyleString(styleToAdd: String): String {
    var style: String? = this.style

    style = if (style == null) styleToAdd else "$style $styleToAdd"

    return style
}