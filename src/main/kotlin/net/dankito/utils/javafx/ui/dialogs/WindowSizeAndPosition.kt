package net.dankito.utils.javafx.ui.dialogs


open class WindowSizeAndPosition(var screenX: Double, var screenY: Double, var width: Double, var height: Double) {

    open val isScreenXSet: Boolean
        get() = screenX.isNaN() == false

    open val isScreenYSet: Boolean
        get() = screenY.isNaN() == false


    open val isWidthSet: Boolean
        get() = width.isNaN() == false && width > 0.0

    open val isHeightSet: Boolean
        get() = height.isNaN() == false && height > 0.0

}