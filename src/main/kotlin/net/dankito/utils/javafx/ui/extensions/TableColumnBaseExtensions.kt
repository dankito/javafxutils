package net.dankito.utils.javafx.ui.extensions

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.scene.control.TableColumnBase
import javafx.scene.control.TableView


fun TableColumnBase<*, *>.addStyleToCurrentStyle(styleToAdd: String) {
    this.style = this.appendStyleToCurrentStyleString(styleToAdd)
}


fun TableColumnBase<*, *>.initiallyUseRemainingSpace(tableView: TableView<*>) {

    val column = this

    val tableWidthChangedListener = object : ChangeListener<Number> {

        override fun changed(observable: ObservableValue<out Number>, oldValue: Number, newValue: Number) {
            if (newValue.toDouble() > 0.0) {
                tableView.widthProperty().removeListener(this)

                var otherColumnsWidth = 0.0

                tableView.columns.forEach { tableColumn ->
                    if (tableColumn != column) {
                        otherColumnsWidth += tableColumn.width
                    }
                }

                column.prefWidth = tableView.width - otherColumnsWidth - 2.0
            }
        }

    }

    tableView.widthProperty().addListener(tableWidthChangedListener)
}