package net.dankito.utils.javafx.ui.dialogs

import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.Fragment


abstract class Window : Fragment() {


    open fun show(title: String? = null, iconUrl: String? = null, position: WindowSizeAndPosition? = null,
             stageStyle: StageStyle = StageStyle.DECORATED, modality: Modality = Modality.NONE, owner: Window? = primaryStage) : Stage {

        val dialogStage = Stage()

        dialogStage.title = title
        iconUrl?.let { setIcon(dialogStage, it) }
        owner?.let { dialogStage.initOwner(it) }

        dialogStage.initModality(modality)
        dialogStage.initStyle(stageStyle)

        val scene = Scene(this.root)

        dialogStage.scene = scene

        position?.let { setPosition(dialogStage, it) }
        ?: dialogStage.centerOnScreen()

        beforeShow(dialogStage)

        dialogStage.show()
        dialogStage.requestFocus()

        this.modalStage = dialogStage

        return dialogStage
    }


    protected open fun setIcon(dialogStage: Stage, iconUrl: String) {
        // TODO: add ImageCache
        dialogStage.icons.add(Image(iconUrl))
    }

    protected open fun setPosition(stage: Stage, position: WindowSizeAndPosition) {
        if (position.isScreenXSet) {
            stage.x = position.screenX
        }

        if (position.isScreenYSet) {
            stage.y = position.screenY
        }

        if (position.isWidthSet) {
            stage.width = position.width
        }

        if (position.isHeightSet) {
            stage.height = position.height
        }
    }

    protected open fun beforeShow(dialogStage: Stage) {
        // may overwritten in subclass
    }

}