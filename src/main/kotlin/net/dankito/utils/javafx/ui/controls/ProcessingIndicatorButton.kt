package net.dankito.utils.javafx.ui.controls

import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import javafx.scene.control.Button
import javafx.scene.layout.HBox
import net.dankito.utils.javafx.ui.extensions.ensureOnlyUsesSpaceIfVisible
import net.dankito.utils.javafx.ui.extensions.fixedHeight
import tornadofx.*


fun EventTarget.processingIndicatorButton(buttonText: String = "Update", fixedHeight: Double? = null, op: ProcessingIndicatorButton.() -> Unit = {})
        = opcr(this, ProcessingIndicatorButton(buttonText, fixedHeight), op)


open class ProcessingIndicatorButton(buttonText: String = "Update", fixedHeight: Double? = null): HBox() {

    protected val isProcessing = SimpleBooleanProperty(false)

    val button: Button


    init {
        this.apply {
            fixedHeight?.let { this.fixedHeight = it }

            prefWidth = 125.0

            progressindicator {
                useMaxHeight = true

                visibleWhen(isProcessing)

                ensureOnlyUsesSpaceIfVisible()

                hboxConstraints {
                    marginRight = 2.0
                }
            }

            this@ProcessingIndicatorButton.button = button(buttonText) {
                useMaxHeight = true
                prefWidthProperty().bind(this@ProcessingIndicatorButton.prefWidthProperty())

                disableWhen(isProcessing)
            }
        }
    }


    open var isDefaultButton: Boolean
        get() = button.isDefaultButton
        set(value) { button.isDefaultButton = value }


    open fun setIsProcessing() {
        isProcessing.value = true
    }

    open fun resetIsProcessing() {
        isProcessing.value = false
    }

    open fun action(op: () -> Unit) {
        button.setOnAction {
            isProcessing.value = true
            op()
        }
    }

}