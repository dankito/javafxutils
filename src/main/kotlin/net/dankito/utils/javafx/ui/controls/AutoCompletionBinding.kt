package net.dankito.utils.javafx.ui.controls

import impl.org.controlsfx.autocompletion.AutoCompletionTextFieldBinding
import impl.org.controlsfx.autocompletion.SuggestionProvider
import impl.org.controlsfx.skin.AutoCompletePopup
import javafx.beans.binding.Bindings
import javafx.scene.control.ContextMenu
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import net.dankito.utils.javafx.ui.extensions.findClickedListCell
import org.controlsfx.control.textfield.AutoCompletionBinding
import org.slf4j.LoggerFactory
import tornadofx.ListCellFragment
import tornadofx.cellFragment
import kotlin.reflect.KClass


open class AutoCompletionBinding<T>(private val textField: TextField, private val suggestionProvider: SuggestionProvider<T> = SuggestionProvider.create(emptyList<T>()))
    : AutoCompletionTextFieldBinding<T>(textField, suggestionProvider) {

    companion object {
        private val log = LoggerFactory.getLogger(net.dankito.utils.javafx.ui.controls.AutoCompletionBinding::class.java)
    }


    open var getContextMenuForItemListener: ((item: T) -> ContextMenu?)? = null

    open var listCellFragment: KClass<out ListCellFragment<T>>? = null

    protected var currentQueryToSelectFromAutoCompletionList: String? = null

    protected var _suggestionList: ListView<T>? = null

    protected var autoCompletionPopup: AutoCompletePopup<T>? = null

    protected var prefItemHeightToSet: Double? = null


    init {
        suggestionProvider.isShowAllIfEmpty = true

        textField.focusedProperty().addListener { _, _, newValue -> textFieldFocusedChanged(newValue) }

        initAutoCompletePopup()
    }

    protected open fun initAutoCompletePopup() {
        try {
            val autoCompletionPopupField = AutoCompletionBinding::class.java.getDeclaredField("autoCompletionPopup")
            autoCompletionPopupField.isAccessible = true

            this.autoCompletionPopup = (autoCompletionPopupField.get(this) as? AutoCompletePopup<T>)

            autoCompletionPopup?.let { autoCompletionPopup ->
                if(autoCompletionPopup.skin != null) {
                    setSuggestionList(autoCompletionPopup.skin.node as? ListView<T>)
                }
                else {
                    setSuggestionList(null)

                    autoCompletionPopup.skinProperty().addListener { _, _, newValue ->
                        setSuggestionList(newValue?.node as? ListView<T>)
                    }
                }

                autoCompletionPopup.consumeAutoHidingEvents = false
            }
        } catch(e: Exception) {
            log.error("Could not initialized autoCompletionPopup", e)
        }
    }


    override fun completeUserInput(completion: T) {
        // avoid calling super.completeUserInput(completion) as otherwise text field's text gets set to string representation, not to entity's title
    }


    open fun setAutoCompleteList(autoCompletionList: Collection<T>, queryToSelectFromAutoCompletionList: String = completionTarget.text) {
        suggestionProvider.clearSuggestions()
        suggestionProvider.addPossibleSuggestions(autoCompletionList)

        currentQueryToSelectFromAutoCompletionList = queryToSelectFromAutoCompletionList

        if (textField.isFocused) { // only show autocompletion list if text field is focused
            setUserInput(queryToSelectFromAutoCompletionList)
        }
    }

    protected open fun textFieldFocusedChanged(isFocused: Boolean) {
        if (isFocused) {
            currentQueryToSelectFromAutoCompletionList?.let {
                setUserInput(it)
            }
        }
        else {
            hidePopup()
        }
    }


    protected open fun setSuggestionList(suggestionList: ListView<T>?) {
        this._suggestionList = suggestionList

        prefItemHeightToSet?.let {
            setPrefItemHeight(it)

            prefItemHeightToSet = null
        }

        suggestionList?.let {
            listCellFragment?.let {
                suggestionList.cellFragment(fragment = it)
            }
        }

        suggestionList?.setOnContextMenuRequested { e ->
            val listCell = e.pickResult?.findClickedListCell<T>()
            listCell?.item?.let { clickedItem ->
                getContextMenuForItemListener?.invoke(clickedItem)?.let { contextMenu ->
                    contextMenu.show(listCell, e.screenX, e.screenY)
                }
            }
        }
    }


    open fun updatePopupPosition() {
        if (autoCompletionPopup?.isShowing == true) {
            hidePopup()

            showPopup()
        }
    }


    override fun showPopup() {
        if (textField.isFocused) { // one way showPopup() gets called is in Platform.runLater() - without checking then if TextField still got the focus -> fix that bug by checking TextField's focus
            super.showPopup()
        }
    }


    open fun setPrefItemHeight(prefHeight: Double) {
        if (_suggestionList == null || autoCompletionPopup == null) {
            // if suggestionListField is still null, set prefItemHeightToSet and wait till suggestionListField is set
            prefItemHeightToSet = prefHeight
        }

        _suggestionList?.let { suggestionList ->
            autoCompletionPopup?.let { autoCompletionPopup ->
                // code copied from impl.org.controlsfx.skin.AutoCompletePopupSkin where LIST_CELL_HEIGHT is set to a fixed value of 24 (how should a fixed item height be generic?)
                suggestionList.prefHeightProperty().bind(
                    Bindings.min(
                        autoCompletionPopup.visibleRowCountProperty(),
                        Bindings.size(suggestionList.items)
                    ).multiply(prefHeight + 5).add(18) // + 5 due to AutoCompleteList internal padding
                )
            }
        }
    }

}