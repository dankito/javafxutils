package net.dankito.utils.javafx.ui.controls

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import tornadofx.opcr



fun EventTarget.intTextfield(property: ObservableValue<Number>, allowNegativeNumbers: Boolean = true, op: IntTextField.() -> Unit = {})
        = opcr(this, IntTextField(property, allowNegativeNumbers), op)



open class IntTextField @JvmOverloads constructor(
                    propertyToBind: ObservableValue<Number>? = null,
                    protected val allowNegativeNumbers: Boolean = true)
    : NumberTextFieldBase(propertyToBind) {


    companion object {
        const val PositiveIntRegexPattern = "[0-9]*"
        val PositiveIntRegex = Regex(PositiveIntRegexPattern)
        val IntRegex = Regex("-?" + PositiveIntRegexPattern)
    }


    override fun isTextInputAllowed(text: String): Boolean {
        return if (allowNegativeNumbers) {
            IntRegex.matches(text)
        }
        else {
            PositiveIntRegex.matches(text)
        }
    }

    override fun convertToString(value: Number): String {
        return Integer.toString(value.toInt())
    }

    override fun convertFromString(value: String): Number? {
        val trimmedValue = value.trim()

        return if (trimmedValue.length < 1) {
            null
        } else Integer.valueOf(trimmedValue)

    }

}