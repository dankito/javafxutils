package net.dankito.utils.javafx.ui.constants


class Texts {

    companion object  {
        const val DefaultAddText = "+"

        const val DefaultRemoveText = "-"
    }

}