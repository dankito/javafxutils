package net.dankito.utils.javafx.ui.controls

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.scene.control.Label
import tornadofx.bind
import tornadofx.opcr
import java.text.NumberFormat


fun EventTarget.currencyLabel(value: ObservableValue<Number>, countDecimalPlaces: Int? = 2, currencyFormat: NumberFormat = CurrencyLabel.CurrencyNumberFormat, op: CurrencyLabel.() -> Unit = {})
        = opcr(this, CurrencyLabel(value, countDecimalPlaces, currencyFormat), op)


open class CurrencyLabel @JvmOverloads constructor(
    value: ObservableValue<Number>,
    countDecimalPlaces: Int? = 2,
    currencyFormat: NumberFormat = CurrencyNumberFormat
) : Label() {

    companion object {

        val CurrencyNumberFormat = NumberFormat.getCurrencyInstance()

    }


    init {
        countDecimalPlaces?.let {
            currencyFormat.minimumFractionDigits = countDecimalPlaces
            currencyFormat.maximumFractionDigits = countDecimalPlaces
        }

        bind(value, true, null, currencyFormat)
    }

}