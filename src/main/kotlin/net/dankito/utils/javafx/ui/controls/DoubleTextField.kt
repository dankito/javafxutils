package net.dankito.utils.javafx.ui.controls

import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import org.slf4j.LoggerFactory
import tornadofx.opcr
import java.text.DecimalFormat
import java.text.NumberFormat


fun EventTarget.doubleTextfield(property: ObservableValue<Number>, doubleFormat: DecimalFormat,
                                allowNegativeNumbers: Boolean = true, op: DoubleTextField.() -> Unit = {})
        = opcr(this, DoubleTextField(property, doubleFormat, allowNegativeNumbers), op)

fun EventTarget.doubleTextfield(property: ObservableValue<Number>, allowNegativeNumbers: Boolean = true,
                                countDecimalPlaces: Int = 2, decimalSeparator: Char? = null, op: DoubleTextField.() -> Unit = {})
        = opcr(this, DoubleTextField(property, allowNegativeNumbers, countDecimalPlaces, decimalSeparator), op)



open class DoubleTextField(propertyToBind: ObservableValue<Number>? = null,
                           protected val doubleFormat: DecimalFormat,
                           allowNegativeNumbers: Boolean = true)
    : NumberTextFieldBase() {


    companion object {

        private fun createDecimalFormat(countDecimalPlaces: Int, decimalSeparator: Char?): DecimalFormat {
            val doubleFormat: DecimalFormat = NumberFormat.getNumberInstance() as DecimalFormat

            doubleFormat.isGroupingUsed = false
            doubleFormat.minimumFractionDigits = countDecimalPlaces
            doubleFormat.maximumFractionDigits = countDecimalPlaces

            decimalSeparator?.let { doubleFormat.decimalFormatSymbols.decimalSeparator = decimalSeparator }

            return doubleFormat
        }

        private val log = LoggerFactory.getLogger(DoubleTextField::class.java)

    }


    @JvmOverloads constructor(property: ObservableValue<Number>? = null, allowNegativeNumbers: Boolean = true,
                              countDecimalPlaces: Int = 2, decimalSeparator: Char? = null)
            : this(property, createDecimalFormat(countDecimalPlaces, decimalSeparator), allowNegativeNumbers)


    protected val isInputAllowedRegex: Regex


    init {
        val decimalSeparator = doubleFormat.decimalFormatSymbols.decimalSeparator
        isInputAllowedRegex = createIsInputAllowedRegex(decimalSeparator, doubleFormat.maximumFractionDigits, allowNegativeNumbers)

        // don't pass propertyToBind to NumberTextFieldBase's constructor as this would call convertToString() at a
        // time doubleFormat is not initialized yet
        bind(propertyToBind)
    }

    protected open fun createIsInputAllowedRegex(decimalSeparator: Char, countDecimalPlaces: Int, allowNegativeNumbers: Boolean): Regex {
        val signs = if (allowNegativeNumbers) "-+" else "+"

        val maskedDecimalSeparator = if (decimalSeparator == '.') "\\." else decimalSeparator.toString()

        val pattern = "[$signs]?[0-9]*$maskedDecimalSeparator?[0-9]{0,${countDecimalPlaces}}"

        return Regex(pattern)
    }


    override fun isTextInputAllowed(text: String): Boolean {
        return isInputAllowedRegex.matches(text)
    }

    override fun convertToString(value: Number): String {
        return doubleFormat.format(value.toDouble())
    }

    override fun convertFromString(value: String): Number? {
        val trimmedValue = value.trim()

        if (trimmedValue.length < 1) {
            return null
        }
        else {
            try {
                return doubleFormat.parse(value)
            } catch (e: Exception) {
                log.debug("Could not parse '$value' to double with NumberFormat $doubleFormat")
            }

            return trimmedValue.toDoubleOrNull()
        }

    }

}