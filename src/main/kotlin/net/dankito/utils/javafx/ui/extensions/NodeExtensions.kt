package net.dankito.utils.javafx.ui.extensions

import javafx.scene.Node
import javafx.scene.layout.AnchorPane
import tornadofx.UIComponent


fun UIComponent.ensureOnlyUsesSpaceIfVisible() {
    this.root.ensureOnlyUsesSpaceIfVisible()
}

fun Node.ensureOnlyUsesSpaceIfVisible() {
    this.managedProperty().bind(this.visibleProperty())
}


fun Node.addStyleToCurrentStyle(styleToAdd: String) {
    this.style = this.appendStyleToCurrentStyleString(styleToAdd)
}


fun UIComponent.setAnchorPaneOverallAnchor(anchorValue: Double) {
    this.root.setAnchorPaneOverallAnchor(anchorValue)
}

fun Node.setAnchorPaneOverallAnchor(anchorValue: Double) {
    AnchorPane.setLeftAnchor(this, anchorValue)
    AnchorPane.setTopAnchor(this, anchorValue)
    AnchorPane.setRightAnchor(this, anchorValue)
    AnchorPane.setBottomAnchor(this, anchorValue)
}