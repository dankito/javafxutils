package net.dankito.utils.javafx.ui.controls

import javafx.event.EventTarget
import javafx.scene.control.Button
import net.dankito.utils.javafx.ui.constants.Fonts
import net.dankito.utils.javafx.ui.constants.Texts
import net.dankito.utils.javafx.ui.constants.UiColors
import net.dankito.utils.javafx.ui.extensions.styleContentAsAddButton
import tornadofx.opcr


fun EventTarget.addButton(addText: String = Texts.DefaultAddText, textColorHex: String = UiColors.AddButtonHexColor,
                          fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize, op: AddButton.() -> Unit = {})
        = opcr(this, AddButton(addText, textColorHex, fontSize), op)


open class AddButton(addText: String = Texts.DefaultAddText, textColorHex: String = UiColors.AddButtonHexColor, fontSize: Double = Fonts.DefaultAddAndRemoveTextFontSize)
    : Button() {

    init {
        styleContentAsAddButton(addText, textColorHex, fontSize)
    }

}