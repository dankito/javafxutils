package net.dankito.utils.javafx.ui.extensions

import javafx.scene.control.MenuItem


fun MenuItem.addStyleToCurrentStyle(styleToAdd: String) {
    this.style = this.appendStyleToCurrentStyleString(styleToAdd)
}