package net.dankito.utils.javafx.util.converter

import javafx.util.converter.PercentageStringConverter
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


/**
 * JavaFX's PercentageStringConverter has a number format with multiplier 100, so that only numbers from 0 - 1 get
 * formatted correctly as percentage.
 *
 * If your numbers range from 0 - 100, use this class instead for formatting percentages.
 */
class ZeroTo100PercentageStringConverter : PercentageStringConverter {

    constructor() : this(Locale.getDefault())

    constructor(locale: Locale) : super(locale)


    override fun getNumberFormat(): NumberFormat {
        val numberFormat = super.getNumberFormat()

        (numberFormat as? DecimalFormat)?.multiplier = 1

        return numberFormat
    }

}