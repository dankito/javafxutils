package net.dankito.utils.javafx.clipboard

import javafx.scene.input.Clipboard
import net.dankito.utils.clipboard.ClipboardContent
import net.dankito.utils.javafx.ui.image.JavaFXImage
import net.dankito.utils.ui.image.Image
import net.dankito.utils.web.UrlUtil
import java.io.File


class JavaFXClipboardContent(private val clipboard: Clipboard, private val urlUtil: UrlUtil) : ClipboardContent() {


    override val hasPlainText: Boolean
        get()  {
            return clipboard.hasString()
        }

    override val plainText: String? = clipboard.string


    override val hasUrl: Boolean
        get() {
            val text = this.plainText
            return clipboard.hasUrl() || (text != null && urlUtil.isHttpUri(text))
        }

    override val url: String?
        get() {
            clipboard.url?.let { return it }

            plainText?.let {
                if(urlUtil.isHttpUri(it)) {
                    return it
                }
            }

            return null
        }


    override val hasHtml: Boolean
        get()  {
            return clipboard.hasHtml()
        }

    override val html: String? = clipboard.html


    override val hasRtf: Boolean
        get()  {
            return clipboard.hasRtf()
        }

    override val rtf: String? = clipboard.rtf


    override val hasImage: Boolean
        get()  {
            return clipboard.hasImage()
        }

    override val image: Image?
        get() {
            clipboard.image?.let {
                return JavaFXImage(clipboard.image)
            }

            return null
        }


    override val hasFiles: Boolean
        get()  {
            return clipboard.hasFiles()
        }

    override val files: List<File> = clipboard.files ?: listOf()

}
